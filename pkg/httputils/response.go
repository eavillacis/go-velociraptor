package httputils

import "bitbucket.org/publipromueve/gaia-cm/pkg/errors"

//Error struct for error messages
type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Details string `json:"details"`
}

// ErrorResponse show error for api calls
func ErrorResponse(err error) *Error {
	apiError, ok := err.(errors.ApiError)

	if ok {
		return &Error{
			Code:    apiError.Code(),
			Message: apiError.Message(),
			Details: err.Error(),
		}
	}

	return &Error{
		Code:    "0",
		Message: "Internal Server Error",
	}
}
