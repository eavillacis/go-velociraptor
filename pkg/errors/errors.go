package errors

import (
	errorsHelpers "github.com/pkg/errors"
)

//APIError is the interface that encapsulates all API HTTP Errors
type APIError interface {
	Code() string
	Message() string
}

//New Generates a New Error
func New(message string) error {
	return errorsHelpers.New(message)
}

//Wrap encapsulates an error for forming stacktrace
func Wrap(err error, message string) error {
	return errorsHelpers.Wrap(err, message)
}
