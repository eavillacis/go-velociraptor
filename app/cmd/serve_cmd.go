package cmd

import (
	"fmt"
	"log"

	"bitbucket.org/publipromueve/go-api/app/api"
	"bitbucket.org/publipromueve/go-api/app/conf"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(serveCmd)
}

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start a new API server",
	Long:  `Start a new API server`,
	Run: func(cmd *cobra.Command, args []string) {
		serve()
	},
}

func serve() {
	config, err := conf.LoadConfig(configFile)
	if err != nil {
		log.Fatalf("Failed to load configuration: %+v", err)
	}

	fmt.Println("config tibi", config)

	api := api.NewAPI(config)

	api.ListenAndServe()
}
