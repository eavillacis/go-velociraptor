package main

import (
	"bitbucket.org/publipromueve/go-api-logs/app/cmd"
)

func main() {
	cmd.Execute()
}
