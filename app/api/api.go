package api

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/publipromueve/go-api/app/conf"

	"github.com/gin-gonic/gin"
)

// API is the struct that contains all api functionality
type API struct {
	config  *conf.Configuration
	handler *gin.Engine
}

// NewAPI mounts all routes
func NewAPI(config *conf.Configuration) *API {
	api := &API{
		config: config,
	}

	r := gin.Default()

	api.handler = r

	api.initRoutes()

	return api
}

// ListenAndServe starts the API
func (a *API) ListenAndServe() {
	host := fmt.Sprintf(":%d", a.config.Port)

	srv := &http.Server{
		Addr:    host,
		Handler: a.handler,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Printf("listen: %s\n", err)
		}
	}()

	log.Printf("Server listening on address %s\n", host)

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}

//ProxyRouter Router type
type ProxyRouter struct {
	address *url.URL
}
