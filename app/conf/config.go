package conf

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

// ServicesConfiguration construct API URLs
type ServicesConfiguration struct {
	ClubMilesURL  string `split_words:"true"`
	AuthorizerURL string `split_words:"true"`
}

// Configuration contains configuration
type Configuration struct {
	Port     int `default:"4000"`
	Services ServicesConfiguration
}

func loadEnvironment(filename string) error {
	var err error
	if filename != "" {
		err = godotenv.Load(filename)
	} else {
		err = godotenv.Load()

		if os.IsNotExist(err) {
			return nil
		}
	}
	return err
}

// LoadConfig loads the configuration from a file
func LoadConfig(filename string) (*Configuration, error) {
	if err := loadEnvironment(filename); err != nil {
		return nil, err
	}

	config := new(Configuration)

	if err := envconfig.Process("API", config); err != nil {
		return nil, err
	}

	return config, nil
}
